package com.revolut.api.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "trns_log")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "trns_id")
    private Long id;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn( name = "from_acc_id")
    private Account fromAccount;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn( name = "to_acc_id")
    private Account toAccount;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "guid")
    private String guid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getFromAccount() {
        return fromAccount;
    }

    public void setFromAccount(Account fromAccountId) {
        this.fromAccount = fromAccountId;
    }

    public Account getToAccount() {
        return toAccount;
    }

    public void setToAccount(Account toAccountId) {
        this.toAccount = toAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }
}
