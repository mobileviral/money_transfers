package com.revolut.api.repository.impl;

import com.revolut.api.entity.Account;
import com.revolut.api.repository.AbstractRepository;
import com.revolut.api.repository.AccountRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class AccountRepositoryImpl extends AbstractRepository<Account,Long> implements AccountRepository  {

    private EntityManagerFactory emf;

    public AccountRepositoryImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public Account findAccountById(Long id) {
        EntityManager em = emf.createEntityManager();
        return find(em, id, Account.class);
    }

    public Account saveAccount(Account account) {
        EntityManager em = emf.createEntityManager();
        return save(em, account);
    }

}
