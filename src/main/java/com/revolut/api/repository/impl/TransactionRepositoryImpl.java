package com.revolut.api.repository.impl;

import com.revolut.api.entity.Account;
import com.revolut.api.entity.Transaction;
import com.revolut.api.exception.ErrorCode;
import com.revolut.api.exception.MyException;
import com.revolut.api.repository.AbstractRepository;
import com.revolut.api.repository.TransactionRepository;
import org.eclipse.jetty.http.HttpStatus;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;

import static com.revolut.api.exception.ErrorCode.INSUFFICIENT_BALANCE;

public class TransactionRepositoryImpl extends AbstractRepository<Transaction, Long> implements TransactionRepository {

    private EntityManagerFactory emf;

    public TransactionRepositoryImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public Transaction registerTransaction(Transaction t) {

        EntityManager em = emf.createEntityManager();

        Transaction saved = findByGuid(em, t.getGuid());

        if (saved != null)
            return saved;

        t.setFromAccount(getDebitedAccount(em, t));
        t.setToAccount(getCreditedAccount(em, t));

        return save(em, t);

    }

    private Transaction findByGuid(EntityManager em, String guid) {

        Transaction entity;

        try {
            entity = em.createQuery("SELECT t FROM Transaction t WHERE guid =:guid", Transaction.class)
                    .setParameter("guid", guid)
                    .getSingleResult();
        } catch (NoResultException e) {
            entity = null;
        }
        return entity;

    }

    private Account getDebitedAccount(EntityManager em, Transaction t) {

        Account from = em.find(Account.class, t.getFromAccount().getId());

        if (from == null)
            throw new MyException(ErrorCode.ACCOUNT_NOT_FOUND, HttpStatus.NOT_FOUND_404, "Account id " + t.getFromAccount().getId() + " does not exist");

        if (!from.hasSufficientBalance(t.getAmount()))
            throw new MyException(INSUFFICIENT_BALANCE, HttpStatus.NOT_FOUND_404, "Not enough balance found for account " + t.getFromAccount().getId());

        from.withdraw(t.getAmount());

        return from;

    }

    private Account getCreditedAccount(EntityManager em, Transaction t) {

        Account to = em.find(Account.class, t.getToAccount().getId());

        if (to == null)
            throw new MyException(ErrorCode.ACCOUNT_NOT_FOUND, HttpStatus.NOT_FOUND_404, "Account id " + t.getToAccount().getId() + " does not exist");

        to.credit(t.getAmount());

        return to;

    }

}
