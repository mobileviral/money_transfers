package com.revolut.api.repository;

import com.revolut.api.entity.Transaction;

public interface TransactionRepository {

    Transaction registerTransaction(Transaction t);

}
