package com.revolut.api.repository;

import com.revolut.api.entity.Account;

public interface AccountRepository {

    Account findAccountById (Long id);

    Account saveAccount (Account account);

}
