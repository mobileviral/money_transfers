package com.revolut.api.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public abstract class AbstractRepository<T, ID> {

    public T save(EntityManager em, T entity) {
        EntityTransaction dbTrns = em.getTransaction();
        try {
            dbTrns.begin();
            em.persist(entity);
            dbTrns.commit();
        } catch (Throwable e) {
            if (dbTrns.isActive())
                dbTrns.rollback();
            throw e;
        } finally {
            em.close();
        }

        return entity;
    }

    public T find(EntityManager em, ID id, Class<T> clazz) {
        T entity = em.find(clazz, id);
        em.close();
        return entity;
    }

}
