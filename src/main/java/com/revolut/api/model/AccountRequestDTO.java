package com.revolut.api.model;

import java.math.BigDecimal;

public class AccountRequestDTO {

    private BigDecimal balance;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
