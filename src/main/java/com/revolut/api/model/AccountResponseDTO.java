package com.revolut.api.model;

import java.math.BigDecimal;
import java.util.Objects;

public class AccountResponseDTO {

    private Long id;

    private BigDecimal balance;

    public AccountResponseDTO() {

    }

    public AccountResponseDTO(Long id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AccountResponseDTO)) return false;
        AccountResponseDTO that = (AccountResponseDTO) o;
        return Objects.equals(getId(), that.getId()) &&
                (getBalance().compareTo(that.getBalance()) == 0);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBalance());
    }
}
