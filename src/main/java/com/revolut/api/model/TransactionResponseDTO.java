package com.revolut.api.model;

import java.math.BigDecimal;
import java.util.Objects;

public class TransactionResponseDTO {

    private Long id;

    private Long fromAccountId;

    private Long toAccountId;

    private BigDecimal amount;

    public TransactionResponseDTO() {

    }

    public TransactionResponseDTO(Long id, Long fromAccountId, Long toAccountId, BigDecimal amount) {
        this.id = id;
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(Long fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public Long getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(Long toAccountId) {
        this.toAccountId = toAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TransactionResponseDTO)) return false;
        TransactionResponseDTO that = (TransactionResponseDTO) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getFromAccountId(), that.getFromAccountId()) &&
                Objects.equals(getToAccountId(), that.getToAccountId()) &&
                (getAmount().compareTo(that.getAmount()) == 0);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFromAccountId(), getToAccountId(), getAmount());
    }
}
