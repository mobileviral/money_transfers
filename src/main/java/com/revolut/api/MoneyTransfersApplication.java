package com.revolut.api;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

public class MoneyTransfersApplication {

    public static void main(String[] args) throws Exception {

        startService();
    }

    public static void startService() throws Exception {

        WebAppContext webAppContext = new WebAppContext();
        webAppContext.setDescriptor("src/main/resources/WEB-INF/web.xml");
        webAppContext.setResourceBase("/");
        webAppContext.setContextPath("/");

        Server server = new Server(8080);
        server.setHandler(webAppContext);

        server.start();
        //server.join();

    }
}
