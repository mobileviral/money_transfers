package com.revolut.api.config;

import com.revolut.api.exception.MyExceptionMapper;
import com.revolut.api.repository.TransactionRepository;
import com.revolut.api.repository.impl.TransactionRepositoryImpl;
import com.revolut.api.repository.AccountRepository;
import com.revolut.api.repository.impl.AccountRepositoryImpl;
import com.revolut.api.service.AccountService;
import com.revolut.api.service.TransactionService;
import org.dozer.DozerBeanMapper;
import org.glassfish.jersey.server.ResourceConfig;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class AppConfig extends ResourceConfig {

    public AppConfig() {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("money-transfers");

        //mapping bean
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.addMapping(new MappingConfig());

        AccountRepository accountRepository = new AccountRepositoryImpl(emf);
        TransactionRepository transactionRepository = new TransactionRepositoryImpl(emf);

        TransactionService transactionService = new TransactionService(transactionRepository, mapper);
        AccountService accountService = new AccountService(accountRepository, mapper);

        register(accountService);
        register(transactionService);
        register(new MyExceptionMapper());
    }

}
