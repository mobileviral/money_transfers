package com.revolut.api.config;

import com.revolut.api.entity.Transaction;
import com.revolut.api.model.TransactionRequestDTO;
import com.revolut.api.model.TransactionResponseDTO;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;

public class MappingConfig extends BeanMappingBuilder {

    @Override
    protected void configure() {

        mapping(TransactionRequestDTO.class, Transaction.class, TypeMappingOptions.mapNull(false))
                .fields("fromAccountId", "fromAccount.id")
                .fields("toAccountId", "toAccount.id");

        mapping(Transaction.class, TransactionResponseDTO.class, TypeMappingOptions.mapNull(false))
                .fields("fromAccount.id", "fromAccountId")
                .fields("toAccount.id", "toAccountId");

    }

}
