package com.revolut.api.service;

import com.revolut.api.entity.Transaction;
import com.revolut.api.exception.MyException;
import com.revolut.api.model.TransactionRequestDTO;
import com.revolut.api.model.TransactionResponseDTO;
import com.revolut.api.repository.TransactionRepository;
import org.apache.commons.lang3.StringUtils;
import org.dozer.DozerBeanMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import static com.revolut.api.exception.ErrorCode.REQUIRED_FIELDS_MISSING;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;

@Path("/transactions")
public class TransactionService {

    private TransactionRepository transactionRepository;

    private DozerBeanMapper mapper;

    public TransactionService(TransactionRepository transactionRepository, DozerBeanMapper mapper) {
        this.transactionRepository = transactionRepository;
        this.mapper = mapper;

    }

    @Path("")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public TransactionResponseDTO registerTransaction(
            @HeaderParam("guid") String guid /*guid serves as idempotency key*/,
            TransactionRequestDTO transactionRequestDTO) {

        validateTransactionRequest(guid, transactionRequestDTO);

        Transaction entity = mapper.map(transactionRequestDTO, Transaction.class);
        entity.setGuid(guid);

        entity = transactionRepository.registerTransaction(entity);

        return mapper.map(
                entity,
                TransactionResponseDTO.class);
    }

    private void validateTransactionRequest(String guid, TransactionRequestDTO r) {
        if (StringUtils.isEmpty(guid))
            throw new MyException(REQUIRED_FIELDS_MISSING, BAD_REQUEST_400, "Guid is missing from header");

        if (r.getFromAccountId() == null || r.getToAccountId() == null || r.getAmount() == null)
            throw new MyException(REQUIRED_FIELDS_MISSING, BAD_REQUEST_400, "Required fields are missing from body");
    }

}
