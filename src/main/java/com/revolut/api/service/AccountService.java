package com.revolut.api.service;

import com.revolut.api.entity.Account;
import com.revolut.api.exception.ErrorCode;
import com.revolut.api.exception.MyException;
import com.revolut.api.model.AccountRequestDTO;
import com.revolut.api.model.AccountResponseDTO;
import com.revolut.api.repository.AccountRepository;
import org.dozer.DozerBeanMapper;
import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import static com.revolut.api.exception.ErrorCode.REQUIRED_FIELDS_MISSING;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;

@Path("/accounts")
public class AccountService {

    private AccountRepository accountRepository;

    private DozerBeanMapper mapper;

    public AccountService(AccountRepository accountRepository, DozerBeanMapper mapper) {
        this.accountRepository = accountRepository;
        this.mapper = mapper;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountResponseDTO getAccount(@PathParam("id") Long id) {

        Account entity = accountRepository.findAccountById(id);

        if (entity == null)
            throw new MyException(ErrorCode.ACCOUNT_NOT_FOUND, HttpStatus.NOT_FOUND_404, "Account id " + id + " does not exist");

        return mapper.map(entity,
                AccountResponseDTO.class);
    }

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AccountResponseDTO registerAccount(AccountRequestDTO accountRequest) {

        validateAccountRequest(accountRequest);

        Account accountEntity = accountRepository.saveAccount(
                mapper.map(accountRequest, Account.class));

        return mapper.map(
                accountEntity,
                AccountResponseDTO.class);
    }

    private void validateAccountRequest(AccountRequestDTO r) {
        if (r.getBalance() == null)
            throw new MyException(REQUIRED_FIELDS_MISSING, BAD_REQUEST_400, "Balance is missing from request");
    }

}

