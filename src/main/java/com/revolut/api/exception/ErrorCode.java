package com.revolut.api.exception;

public enum ErrorCode {

    REQUIRED_FIELDS_MISSING(1),
    ACCOUNT_NOT_FOUND(2),
    INSUFFICIENT_BALANCE(3);

    int value;

    ErrorCode(int v){
        value = v;
    }
}