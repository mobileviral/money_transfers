package com.revolut.api.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class MyExceptionMapper implements ExceptionMapper<MyException> {

    @Override
    public Response toResponse(MyException ex) {
            return Response.status(ex.getStatus())
                    .entity(new FaultInfo(ex.getCode().value,ex.getMessage()))
                    .type(MediaType.APPLICATION_JSON)
                            .build();

    }
}
