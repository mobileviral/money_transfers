package com.revolut.api.exception;

public class MyException extends RuntimeException{

    private ErrorCode code;

    private int status;

    public MyException(ErrorCode code, int status, String message) {
        super(message);
        this.code = code;
        this.status = status;
    }

    public ErrorCode getCode() {
        return code;
    }

    public void setCode(ErrorCode code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
