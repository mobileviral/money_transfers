package com.revolut.api.service.test.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revolut.api.model.AccountRequestDTO;
import com.revolut.api.model.AccountResponseDTO;
import com.revolut.api.model.TransactionRequestDTO;
import com.revolut.api.model.TransactionResponseDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.UUID;

public class RequestHelper {

    private static final String baseUrl = "http://localhost:8080/api/v1.0";

    private ObjectMapper objectMapper;

    private HttpClient client;

    public RequestHelper() {
        this.objectMapper = new ObjectMapper();
        this.client = HttpClientBuilder.create().build();;
    }

    public AccountResponseDTO createAccount(BigDecimal balance) throws IOException {

        HttpPost req = new HttpPost(baseUrl + "/accounts");
        req.addHeader("Content-Type", MediaType.APPLICATION_JSON);

        AccountRequestDTO accountRequestDTO = new AccountRequestDTO();
        accountRequestDTO.setBalance(balance);

        req.setEntity(new StringEntity(
                objectMapper.writeValueAsString(accountRequestDTO)));

        HttpResponse res = client.execute(req);

        return objectMapper.readValue(res.getEntity().getContent(),
                AccountResponseDTO.class);

    }

    public TransactionResponseDTO createTransaction(Long from, Long to, BigDecimal amount) throws IOException {

        HttpPost req = new HttpPost(baseUrl + "/transactions");
        req.addHeader("Content-type", MediaType.APPLICATION_JSON);
        req.addHeader("Guid", UUID.randomUUID().toString());

        TransactionRequestDTO transactionRequestDTO = new TransactionRequestDTO(from, to, amount);

        req.setEntity(new StringEntity(
                objectMapper.writeValueAsString(transactionRequestDTO)));

        HttpResponse res = client.execute(req);

        return objectMapper.readValue(res.getEntity().getContent(),
                TransactionResponseDTO.class);

    }

    public AccountResponseDTO getAccount(Long id) throws IOException {

        HttpGet req = new HttpGet(baseUrl + "/accounts/" + id);

        HttpResponse res = client.execute(req);

        return objectMapper.readValue(res.getEntity().getContent(),
                AccountResponseDTO.class);

    }
}
