package com.revolut.api.service.test;

import com.revolut.api.MoneyTransfersApplication;
import com.revolut.api.model.AccountResponseDTO;
import com.revolut.api.model.TransactionResponseDTO;
import com.revolut.api.service.test.utils.RequestHelper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class MoneyTransfersTest {

    private RequestHelper helper;

    @Before
    public void startServer() throws Exception {
        MoneyTransfersApplication.startService();
        helper = new RequestHelper();
    }

    @Test
    public void testRegisterTransaction() throws IOException {

        AccountResponseDTO from = helper.createAccount(BigDecimal.valueOf(10));
        assertEquals(new AccountResponseDTO(1L, BigDecimal.valueOf(10)), from);

        AccountResponseDTO to = helper.createAccount(BigDecimal.valueOf(10));
        assertEquals(new AccountResponseDTO(2L, BigDecimal.valueOf(10)), to);

        TransactionResponseDTO trns = helper.createTransaction(from.getId(), to.getId(), BigDecimal.valueOf(3));
        assertEquals(new TransactionResponseDTO(1L, from.getId(), to.getId(), BigDecimal.valueOf(3)), trns);

        AccountResponseDTO updatedFrom = helper.getAccount(from.getId());
        assertEquals(new AccountResponseDTO(from.getId(), from.getBalance().subtract(trns.getAmount())), updatedFrom);

        AccountResponseDTO updatedTo = helper.getAccount(to.getId());
        assertEquals(new AccountResponseDTO(to.getId(), to.getBalance().add(trns.getAmount())), updatedTo);

    }

}
